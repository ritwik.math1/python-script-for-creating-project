#!/bin/bash

if [ ! -d "venv" ]; then
    # Create a new venv
    echo "Creating new venv..."
    printf "\n"
    python3 -m venv venv
    printf "\n"
fi

# Activate the venv
echo "Activating venv..."
printf "\n"
source venv/bin/activate

echo "Installing dependencies..."
printf "\n"
pip3 install -r requirements.txt
printf "\n"

echo "Executing python code..."
printf "\n"
python3 resolve.py

# Deactivate the venv
echo "Deactivating venv..."
printf "\n"
deactivate
