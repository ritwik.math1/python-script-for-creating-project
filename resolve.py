import stripe
import requests
from pymongo import MongoClient, ReturnDocument
from bson.objectid import ObjectId
import json
import time
db_string = 'mongodb+srv://%s:%s@cluster0.tw0yq.mongodb.net/?retryWrites=true&w=majority' % ('pypa', 'PyPaCC')
accounts_db = 'dev_accounts_ritwik2'
plans_db = 'dev_plans_ritwik2'
user_email = 'ritwik.math@codeclouds.com'
country = 'AU' # None can be assigned if organization country doesn't need to be changed
org_id = None
token = None
stripe.api_key = 'sk_test_51MjcQhDu4iLNSsDaaaSkBRzJEIAb6MRXLV3wNb8ciiidldTuxoLuqNeV8mWUr4lpLvC4wv378x2cZT4IBLgxBTWj00628QDhOS'
base_url_accounts = 'http://localhost:5001'
base_url_plans = 'http://localhost:5002'
client = MongoClient(db_string)
with client.start_session() as session:
    try:
        print('Staring session', end='\n')
        with session.start_transaction():
            print("Transaction started.", end='\n')
            user = session.client[accounts_db].users.find_one_and_update({'email': user_email}, {
                '$set': {'unknown_device_bypass': True}
            }, upsert=True, return_document = ReturnDocument.AFTER)
            if len(user.get('organizations')) <= 1:
                org_id = user.get('organizations')[0].get('id')
                organization = session.client[plans_db].organizations.find_one({
                    '_id': org_id
                })
                user_response = input(f"Continue with {organization['name']} (y/n)")
                if user_response in ['n', 'N']:
                    raise Exception('Cancelled')
            else:
                orgs = []
                count = 0
                for org in user.get('organizations'):
                    organization = session.client[plans_db].organizations.find_one({
                        '_id': org['id']
                    })
                    print(f"{count+1}: {organization['name']}")
                    orgs.append(org['id'])
                    count += 1
                user_response = input(f"Select organization \n")
                org_id = orgs[int(user_response) - 1]
            subscription = session.client[accounts_db].subscriptions.find_one({
                '$and': [
                    {'org_ids': org_id},
                    {'status': True}
                ]
            })
            if subscription:
                try:
                    stripe.Subscription.delete(subscription.get('gateway_subscription_id'))
                    print(f"Subscription {subscription.get('gateway_subscription_id')} deleted from stripe", end='\n')
                except:
                    print(f"{subscription.get('gateway_subscription_id')} may be in different stripe account", end='\n')
            session.client[accounts_db].app_registrations.delete_many({'org_id': org_id})
            session.client[plans_db].app_registrations.delete_many({'org_id': org_id})
            session.client[plans_db].card_details.delete_many({'org_ids': org_id})
            session.client[plans_db].subscriptions.delete_many({'org_ids': org_id})
            session.client[accounts_db].subscriptions.delete_many({'org_ids': org_id})
            if country:
                session.client[plans_db].organizations.update_one({'_id': org_id}, {'$set': { 'country': country}})
                session.client[accounts_db].organizations.update_one({'_id': org_id}, {'$set': { 'country': country}})
        headers = {
            'Content-Type': 'application/json',
            'x-api-key': 'AIzaSyDkgzduYQPa-Avs2AtPqWbkLIORRGdzXuI'
        }
        data = {
            'email': user_email,
            'password': 'Test@1234',
            'host_data': {
                'fingerprint': ''
            }
        }
        login_response = requests.post(f'{base_url_accounts}/sign-in', headers=headers, json=data)
        token = json.loads(login_response.text).get('result').get('access_token')

        headers = {
            'Authorization': f'Bearer {token}',
            'Content-Type': 'application/json',
            'x-api-key': 'AIzaSyDkgzduYQPa-Avs2AtPqWbkLIORRGdzXuI'
        }
        data = {
            'app_slug': 'hr'
        }
        response = requests.post(f'{base_url_accounts}/app-registrations/create', headers=headers, json=data)
        if response.status_code != 200:
            raise Exception(response.text)
        time.sleep(4)
        yet_another_response = requests.get(f'{base_url_plans}/subscriptions/list', headers=headers)
        if yet_another_response.status_code != 200:
            raise Exception(yet_another_response.text)
        print(json.dumps(json.loads(yet_another_response.text), indent=4), end='\n')
        session.client[accounts_db].users.update_one({'email': user_email}, {
            '$set': {'unknown_device_bypass': False}
        })
        print("Transaction committed successfully.", end='\n')
        session.commit_transaction()
    except Exception as ex:
        print(ex, end='\n')
        print('Aborting session. Reverting back db changes', end='\n')
        session.abort_transaction()
    finally:
        session.end_session()
        print('Ending session', end='\n')