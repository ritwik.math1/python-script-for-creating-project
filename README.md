# Readme

This repository contains a `run.bash` script that you can execute. Here are the steps to make the script executable and run it:

1. Open a terminal or command prompt.
2. Navigate to the directory where the `run.bash` script is located.
3. Run the following command to make the script executable:

   ```shell
   sudo chmod +x ./run.bash

4. After making the script executable, you can run it using the following command

   ```shell
   ./run.bash